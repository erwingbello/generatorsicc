(function() {
    'use strict';

    angular
        .module('siccApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider', 'paginationConstants'];

    function stateConfig($stateProvider, paginationConstants) {
        $stateProvider
        .state('<%= estadoSlug %>', {
            parent: '<%= inspeccionSlug %>.edit',
            url: '/estado?pagina&orden&busqueda&limite',
            data: {
                permissions: ['<%= inspeccionEntityManagementPermission %>'],
                
                pageTitle: '<%= estadoEntityLabelPlural %>'
            },
            views: {
                'dialog-content@': {
                    templateUrl: 'app/entities/_components/entity-list/templates/list-base.html',
                    controller: '<%= estadoEntityClassCase %>Controller',
                    controllerAs: 'vm'
                }
            },
            ncyBreadcrumb: {
                label:'Estados'
            },
            params: {
                pagina: {
                    value: '1',
                    squash: true
                },
                orden: {
                    value: 'id,asc',
                    squash: true
                },
                busqueda: null,
                limite: {
                    value: paginationConstants.itemsPerPage.toString(),
                    squash: true
                }
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.pagina),
                        sort: $stateParams.orden,
                        predicate: PaginationUtil.parsePredicate($stateParams.orden),
                        ascending: PaginationUtil.parseAscending($stateParams.orden),
                        search: $stateParams.busqueda,
                        size: parseInt($stateParams.limite)
                    };
                }],
                <%= inspeccionEntityCammelCase %>: ['entity', function(entity){
                    return entity;
                }],
                estadosListListener:['EntityListener', function(EntityListener){
                    return EntityListener.make();
                }]
            }
        })
        .state('<%= estadoSlug %>.new', {
            parent: '<%= estadoSlug %>',
            url: '/new',
            data: {
                permissions: ['<%= inspeccionEntityManagementPermission %>']
                
            },
            views: {
                'dialog-content@': {
                    templateUrl: 'app/entities/_components/entity-dialog/templates/dialog-base.html',
                    controller: '<%= estadoEntityClassCase %>DialogController',
                    controllerAs: 'vm'
                }
            },
            ncyBreadcrumb: {
                label: 'Nuevo'
            },
            resolve: {
                entity: ['<%= estadoEntityClassCase %>', function(<%= estadoEntityClassCase %>) {
                    return new <%= estadoEntityClassCase %> ({
                        kilometro: null,
                        //SICC:TODO verificar que campos estan definidos para el estado
                        observacionesInspeccion: null,
                        observacionesVerificacion: null,
                        cumple: null,
                        estado: null,
                        id: null
                    });
                }],
                inspeccionEntity: ['<%= inspeccionEntityCammelCase %>', function(<%= inspeccionEntityCammelCase %>){
                    return <%= inspeccionEntityCammelCase %>;<% if(entityWithMedidas){ %>
                }],
                medidasListListener:['EntityListener', function(EntityListener){
                    return EntityListener.make();
                }]<% }  %><% if(!entityWithMedidas){ %>
                }]<% }  %>
            }
        })<% if(entityWithBulk){ %>
        .state('<%= estadoSlug %>.carga-masiva', {
            parent: '<%= estadoSlug %>',
            url: '/carga-masiva',
            data: {
                permissions: ['carga_masiva', 'is_interventor_editor']
                
            },
            ncyBreadcrumb: {
                skip: true // Never display this state in breadcrumb.
            },
            onEnter: ['$stateParams', '$state', '$uibModal', '$rootScope', 'BulkFileUploadControllerConfig', 'estadosListListener', function($stateParams, $state, $uibModal, $rootScope, BulkFileUploadControllerConfig, estadosListListener){
                this.modal=$uibModal.open({
                    templateUrl: 'app/entities/_components/entity-file-uploader/templates/modal-file-upload.html',
                    controller: 'EntityFileUploadController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'md',
                    resolve: {
                        controllerConfig: function() {
                            return new BulkFileUploadControllerConfig({
                                resourceURL: 'api/carga-masiva-<%= indicatorSlug %>',
                                uploadSuccessEventName: 'siccApp:<%= estadoEntityCammelCase %>Update',
                                fileParamName: 'archivo',
                                title:'Carga masiva de estados de la inspección',
                                requestParams:{
                                    idRegistro:$stateParams.id
                                }
                            });
                        }
                    }
                });
                this.modal.result.then(function(){
                    estadosListListener.dispatch();
                    $state.go('^');
                }, function(reason){
                    estadosListListener.dispatch();
                    if(reason==='force-close') return;
                    $state.go('^');
                });
            }],
            onExit:function(){
                var that=this;
                //forzar cierre por cambio de url, y no dejar el dialogo abierto
                that.modal.opened.then(function(){
                    //forzar cierre por si se instancia modal porque la url 
                    //y simultaneamente se hace redirect a otro state (bloqueo de acceso) ya que no se alcanza a cerrar
                    that.modal.dismiss('force-close');
                });
                
            }
        })<% } %>
        .state('<%= estadoSlug %>.edit', {
            parent: '<%= estadoSlug %>',
            url: '/{id_<%= estadoEntitySnakeCase %>}',
            data: {
                permissions: ['<%= inspeccionEntityManagementPermission %>']
                
            },
            views: {
                'dialog-content@': {<% if(entityWithMedidas){ %>
                    templateUrl: 'app/entities/_components/entity-dialog/templates/estado-dialog-medidas.html'
                },
                'form-content@<%= estadoSlug %>.edit': {
                <% }  %>
                    templateUrl: 'app/entities/_components/entity-dialog/templates/dialog-base.html',
                    controller: '<%= estadoEntityClassCase %>DialogController',
                    controllerAs: 'vm'<% if(entityWithMedidas){ %>
                },
                'medidas-content@<%= estadoSlug %>.edit': {
                    templateUrl: 'app/entities/_components/entity-dialog/templates/grid-medidas-base.html',
                    controller: '<%= estadoEntityClassCase %>MedidasController',
                    controllerAs: 'vm'
                }<% }  %><% if(!entityWithMedidas){ %>
                }<% }  %>
            },
            ncyBreadcrumb: {
                label: 'Editar-Ver'
            },
            resolve: {
                entity: ['$stateParams', '<%= estadoEntityClassCase %>', function($stateParams, <%= estadoEntityClassCase %>) {
                    return <%= estadoEntityClassCase %>.get({id : $stateParams.id_<%= estadoEntitySnakeCase %>}).$promise;
                }],
                inspeccionEntity: ['<%= inspeccionEntityCammelCase %>', function(<%= inspeccionEntityCammelCase %>){
                    return <%= inspeccionEntityCammelCase %>;<% if(entityWithMedidas){ %>
                }],
                medidasListListener:['EntityListener', function(EntityListener){
                    return EntityListener.make();
                }]<% }  %><% if(!entityWithMedidas){ %>
                }]<% }  %>
            }<% if(!entityWithMedidas){ %>
        })
        .state('<%= estadoSlug %>-confirmation', {
            parent: '<%= estadoSlug %>.edit',
            url: '/confirmacion',
            data: {
                permissions: ['<%= inspeccionEntityManagementPermission %>'],
                
                pageTitle: 'Confirmación'
            },
            views: {
                'dialog-content@': {
                    templateUrl: 'app/entities/_components/entity-confirmation/templates/confirmation-base.html',
                    controller: 'EstadoEntityConfirmationController',
                    controllerAs: 'vm'
                }
            },
            ncyBreadcrumb: {
                label: 'Confirmación'
            },
            resolve: {
                controllerConfig: ['EntityConfirmationControllerConfig', 'entity', 'inspeccionEntity', '<%= estadoEntityClassCase %>', function(EntityConfirmationControllerConfig, entity, inspeccionEntity, <%= estadoEntityClassCase %>) {
                    return new EntityConfirmationControllerConfig({
                        entity: entity,
                        entityService: <%= estadoEntityClassCase %>,
                        entityName: '<%= estadoSlug %>',
                        parentEntity: inspeccionEntity
                    });
                }]
            }<% }  %>
        });
    }

})();


