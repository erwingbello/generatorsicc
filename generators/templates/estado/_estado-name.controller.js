(function() {
    'use strict';

    angular
        .module('siccApp')
        .controller('<%= estadoEntityClassCase %>Controller', <%= estadoEntityClassCase %>Controller);

    <%= estadoEntityClassCase %>Controller.$inject = ['EstadosListControllerFactory', '$scope', 'pagingParams', '<%= estadoEntityClassCase %>', '<%= inspeccionEntityCammelCase %>', 'estadosListListener'];

    function <%= estadoEntityClassCase %>Controller (EstadosListControllerFactory, $scope, pagingParams, <%= estadoEntityClassCase %>, <%= inspeccionEntityCammelCase %>, estadosListListener) {
        var EstadosListController=EstadosListControllerFactory.create($scope, pagingParams, <%= estadoEntityClassCase %>, <%= inspeccionEntityCammelCase %>, estadosListListener);
        var controller=new EstadosListController({
            title:"<%= estadoEntityLabelPlural %>",
            entityName:"<%= estadoSlug %>",
            parentEntityType:"<%= inspeccionSlug %>",
            sortable:true,<% if(!entityWithBulk){ %>
            withBulkButton:false,<% } %>
            getColumnsConfig:function(controller){
                return {
                    fields:[
                        {
                            sortBy:'id', 
                            label:"ID", 
                            value:function(item){
                                return item.id;
                            }
                        },
                        {
                            sortBy:'estado', 
                            label:"Estado", 
                            value:function(item){
                                return item.estado;
                            }
                        },
                        {
                            sortBy:'cumple', 
                            label:"¿Ha cumplido?", 
                            value:function(item){
                                return item.cumple;
                            },
                            asIndicadorCumple:true
                        },
                        {
                            sortBy:'kilometro', 
                            label:"Kilómetro", 
                            value:function(item){
                                return item.kilometro;
                            }
                        }
                        //SICC:TODO agregar campos adicionales listado de estados
                    ],
                    editLink:function(item){
                        return{
                            state:controller.getOptions().entityName+'.edit',
                            stateParams:{id_<%= estadoEntitySnakeCase %>:item.id}
                        };   
                    }
                };
            }
        });
        
        return controller;
    }
})();
