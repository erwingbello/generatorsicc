(function() {
    'use strict';

    angular
        .module('siccApp')
        .controller('<%= estadoEntityClassCase %>DialogController', <%= estadoEntityClassCase %>DialogController);
    <%= estadoEntityClassCase %>DialogController.$inject = [<% if (entityWithMedidas){ %>'DialogEstadosWithMedidasControllerFactory'<% } %><% if (!entityWithMedidas){ %>'DialogEstadosControllerFactory'<% } %>, '$scope', 'entity', '<%= estadoEntityClassCase %>', 'inspeccionEntity', 'DateUtils'<% if (entityWithMedidas){ %>, 'medidasListListener'<% } %><% if (!entityWithMedidas){ %>, 'DialogKMAbscisaValidationImplement'<% } %>];

    function <%= estadoEntityClassCase %>DialogController (<% if (entityWithMedidas){ %>DialogEstadosWithMedidasControllerFactory<% } %><% if (!entityWithMedidas){ %>DialogEstadosControllerFactory<% } %>, $scope, entity, <%= estadoEntityClassCase %>, inspeccionEntity, DateUtils<% if (entityWithMedidas){ %>, medidasListListener<% } %><% if (!entityWithMedidas){ %>, DialogKMAbscisaValidationImplement<% } %>) {
        var <% if (entityWithMedidas){ %>DialogEstadosWithMedidasController<% } %><% if (!entityWithMedidas){ %>DialogEstadosController<% } %>=<% if (entityWithMedidas){ %>DialogEstadosWithMedidasControllerFactory<% } %><% if (!entityWithMedidas){ %>DialogEstadosControllerFactory<% } %>.create($scope, <%= estadoEntityClassCase %>, entity, inspeccionEntity<% if (entityWithMedidas){ %>, medidasListListener<% } %>);
        <% if (!entityWithMedidas){ %>DialogKMAbscisaValidationImplement.implement(DialogEstadosController);<% } %>
        <% if (entityWithMedidas){ %>DialogEstadosWithMedidasController<% } %><% if (!entityWithMedidas){ %>DialogEstadosController<% } %>.prototype=angular.extend(<% if (entityWithMedidas){ %>DialogEstadosWithMedidasController<% } %><% if (!entityWithMedidas){ %>DialogEstadosController<% } %>.prototype,{
            
            loadParams:function(){
                //var vm=this;
                //SICC:TODO Verificar si el encabezado debe cargar en el scope vm dominios que vienen del servicio params
                
            },
            beforeSave:function(){
                //SICC:TODO Verificar si el nombre del campo  que relaciona el estado con la inspeccion
                entity.estadoDeInspeccion = inspeccionEntity;
            },
            
            getTramoInspeccion:function(){
                // SICC:TODO Verificar en nombre del campo tramoDeInspeccionXXX en el controller de estado
                return inspeccionEntity.tramoDe<%= inspeccionEntityClassCase %>;
            },
            startWatcherIfCanToSaveEntity:function(){
                
                var vm=this;
                $scope.$watchGroup([
                    //SICC:TODO Verificar que campos se deben observar para que se habiliten los botones guardar/finalizar
                    'vm.'+vm.entityCamelCase+'.kilometro',
                    'vm.'+vm.entityCamelCase+'.cumple'
                ], function(){
                    vm.isInvalidToSave=false;
                    vm.showUserToSaveMessage=false;
                    var entityForm=$scope.editForm;

                    // if(!vm.isEstadoInVerification()){
                    //     if(!entityForm || 
                    //         entityForm.kilometro.$invalid
                    //          ){
                    //         vm.isInvalidToSave=true;
                    //     }
                    // }
                    // else{
                    //     if(!entityForm || 
                    //         entityForm.kilometro.$invalid
                    //          ){
                    //         vm.isInvalidToSave=true;
                    //     }
                    // }
                    // if(!entityForm ||  entityForm.kilometro.$invalid){
                    if(!entityForm ||  entityForm.$invalid){
                        vm.isInvalidToSave=true;
                    }

                    if(entityForm && !entityForm.$pristine){
                        vm.showUserToSaveMessage=true;
                    }
                    
                });   
            }<% if (!entityWithMedidas){ %>,
            initCustomValidations:function(){
                //SICC:TODO Verificar si este bloque es necesario, que permite realizar la validacion de los campos km y abscisa y cuales nombres de campos son los que corresponden
                var vm=this;
                vm.setTramo(vm.getTramoInspeccion());
                vm.addKmAbscisaValidation('kilometro','abscisa');  //validaciones DialogKMAbscisaValidationImplement
            }<% } %>
            


        });

        
        var controller=new <% if (entityWithMedidas){ %>DialogEstadosWithMedidasController<% } %><% if (!entityWithMedidas){ %>DialogEstadosController<% } %>({
            entityName:"<%= estadoSlug %>"<% if (!entityWithMedidas){ %>,
            withFinalizeState:true<% } %>
        });
        return controller;
    }
})();
