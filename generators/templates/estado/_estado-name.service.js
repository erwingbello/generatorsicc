(function() {
    'use strict';
    angular
        .module('siccApp')
        .factory('<%= estadoEntityClassCase %>', <%= estadoEntityClassCase %>);

    <%= estadoEntityClassCase %>.$inject = ['$resource'];

    function <%= estadoEntityClassCase %> ($resource) {
        var resourceUrl =  'api/<%= estadoSlug %>/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { 
                method:'PUT',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'finalize':{
                method:'PUT',
                url:'api/<%= estadoSlug %>/finalize'
            },
            'params':{ //SICC-TODO verificar si este estado tiene params
                method: 'GET',
                url:'api/<%= estadoSlug %>/params',
                cache:true<% if(entityWithMedidas){ %>
            },
            'medidas':{
                method:'GET',
                isArray: true,
                url:'api/<%= estadoSlug %>/:id/medidas'
            },
            'saveMedidas':{
                method:'POST',
                url:'api/<%= estadoSlug %>/:id/medidas',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            }<% }  %><% if(!entityWithMedidas){ %>
            }<% }  %>
        });
    }
})();
