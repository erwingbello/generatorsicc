(function () {
    'use strict';

    angular
        .module('siccApp')
        .controller('<%= estadoEntityClassCase %>MedidasController', <%= estadoEntityClassCase %>MedidasController);

    <%= estadoEntityClassCase %>MedidasController.$inject = ['$scope', 'entity', '<%= estadoEntityClassCase %>', 'inspeccionEntity', 'ENTITY_STATES', 'DialogMedidasGridControllerFactory', 'medidasListListener'];

    function <%= estadoEntityClassCase %>MedidasController($scope, entity, <%= estadoEntityClassCase %>, inspeccionEntity, ENTITY_STATES, DialogMedidasGridControllerFactory, medidasListListener) {
        var MedidasGridController = DialogMedidasGridControllerFactory.create($scope, entity, <%= estadoEntityClassCase %>, inspeccionEntity, medidasListListener);

        MedidasGridController.prototype = angular.extend(MedidasGridController.prototype, {
            postConstructor: function () {
                var vm = this;
                vm.cols = [
                    vm.makeColConfig( //SICC:TODO Verificar si la grilla tiene el campo abscisa
                        "abscisa", "Abscisa", "number", 3,  true, false, undefined,
                        {
                            dataTypeValidation: function(){
                                return {
                                    max: vm.abscisaMax,
                                    min: vm.abscisaMin,
                                    step: 1
                                };
                            },
                            isDisabled: function (row) {
                                return vm.isEstadoInVerification();
                            }
                        }
                    )
                    

                    /**
                        arguments:
                        fieldId String
                        fieldLabel Sting
                        fieldType 'number' || 'select' || 'text'
                        bootstrapCols int (0-11)
                        isRequired boolean
                        isDisabled boolean
                        showInVerfication undefined true false (undefined = always)
                        overridesOrCustoms:{}
                    **/
                    /*, //SICC:TODO remover ejemplo de configuracion de campos de la grilla
                    vm.makeColConfig(
                        "tipoDano", "Daño", "select", 3, true, false, undefined,
                        {
                            domainDanio: true
                        }
                    ),
                    vm.makeColConfig(
                        "largoInspeccion", "Largo(m)", "number", 2, true, false, false,
                        {
                            dataTypeValidation: function(){
                                return {
                                    max: "100",
                                    min: "0",
                                    step: 0.01
                                };
                            },
                            dataForCalculableInput: true,
                            calculableFunction: function (bool, row) {
                                if(bool && row) 
                                    row.areaInspeccion = row.anchoInspeccion * row.largoInspeccion;
                            }
                        }
                    ),
                    vm.makeColConfig(
                        "anchoInspeccion", "Ancho(m)", "number", 2, undefined, false, false,
                        {
                            dataTypeValidation: function(){
                                return {
                                    max: "100",
                                    min: "0",
                                    step: 0.01
                                };
                            },
                            isDisabled: function (row) {
                                if (row.tipoDano === 'FML' || row.tipoDano === 'FB' || row.tipoDano === 'PC') {
                                    return false;
                                }
                                row.anchoInspeccion = null;  
                                row.areaInspeccion = null;  
                                return true;
                            },
                            isRequired: function (row) {
                                if (row.tipoDano === 'FML' || row.tipoDano === 'FB' || row.tipoDano === 'PC')
                                    return true;
                                return false;
                            },
                            dataForCalculableInput: true,
                            calculableFunction: function (bool, row) {
                                if(bool && row) 
                                    row.areaInspeccion = row.anchoInspeccion * row.largoInspeccion;
                            }
                        }
                    ),
                    vm.makeColConfig(
                        "areaInspeccion", "Area", "number", 2, undefined, true, false,
                        {
                            dataTypeValidation: function(){
                                return {
                                    max: "10000",
                                    min: "0",
                                    step: 0.01
                                };
                            },
                            isRequired: function (row) {
                                if (row.tipoDano === 'FML' || row.tipoDano === 'FB' || row.tipoDano === 'PC') 
                                    return true;
                                return false;
                            }
                        }
                    ),
                    vm.makeColConfig(
                        "largoVerificacion", "Largo(m)", "number", 2, true, false, true,
                        {
                            dataTypeValidation: function(){
                                return {
                                    max: "100",
                                    min: "0",
                                    step: 0.01
                                };
                            },
                            dataForCalculableInput: true,
                            calculableFunction: function (bool, row) {
                                if(bool && row) 
                                    row.areaVerificacion = row.anchoVerificacion * row.largoVerificacion;
                            }
                        }
                    ),
                    vm.makeColConfig(
                        "anchoVerificacion", "Ancho(m)", "number", 2, undefined, undefined, true,
                        {
                            dataTypeValidation: function(){
                                return {
                                    max: "100",
                                    min: "0",
                                    step: 0.01
                                };
                            },
                            isDisabled: function (row) {
                                if (row.tipoDano === 'FML' || row.tipoDano === 'FB' || row.tipoDano === 'PC') {
                                    return false;
                                }
                                row.anchoVerificacion = null;  
                                row.areaVerificacion = null;  
                                return true;
                            },
                            isRequired: function (row) {
                                if (row.tipoDano === 'FML' || row.tipoDano === 'FB' || row.tipoDano === 'PC')
                                    return true;
                                return false;
                            },
                            dataForCalculableInput: true,
                            calculableFunction: function (bool, row) {
                                if(bool && row) 
                                    row.areaVerificacion = row.anchoVerificacion * row.largoVerificacion;
                            }
                        }
                    ),
                    vm.makeColConfig(
                        "areaVerificacion", "Area", "number", 2, undefined, true, true,
                        {
                            dataTypeValidation: function(){
                                return {
                                    max: "10000",
                                    min: "0",
                                    step: 0.01
                                };
                            },
                            isRequired: function (row) {
                                if (row.tipoDano === 'FML' || row.tipoDano === 'FB' || row.tipoDano === 'PC') 
                                    return true;
                                return false;
                            }
                        }
                    ), {
                        field: "action",
                        title: "",
                        dataType: "command",
                        bootstrapCols: "col-xs-1",
                        show:function(){
                            return !vm.isFinalized();
                        }
                    }*/
                ];
                
            }, 
            loadAditionalDomains: function() {
                //var vm = this;
                //SICC:TODO exponer dominios al vm scope del controller
                
            }
        });

        var controller = new MedidasGridController({
            title: 'Medidas del estado:'
        });

        return controller;
    }
})();
