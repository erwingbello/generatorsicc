(function () {
    'use strict';

    angular
        .module('siccApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider', 'paginationConstants'];

    function stateConfig($stateProvider, paginationConstants) {
        $stateProvider
            .state('<%= inspeccionEntityHyphenCase %>', {
                parent: 'entity',
                url: '/<%= inspeccionEntityHyphenCase %>?page&sort&search&size',
                data: {
                    permissions: ['<%= inspeccionEntityManagementPermission %>'],
                    pageTitle: '<%= inspeccionEntityLabelPlural %>'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/_components/entity-list/templates/list-base.html',
                        controller: '<%= inspeccionEntityClassCase %>Controller',
                        controllerAs: 'vm'
                    }
                },
                ncyBreadcrumb: {
                    skip: true
                },
                params: {
                    page: {
                        value: '1',
                        squash: true
                    },
                    sort: {
                        value: 'id,asc',
                        squash: true
                    },
                    search: null,
                    size: {
                        value: paginationConstants.itemsPerPage.toString(),
                        squash: true
                    }
                },
                resolve: {
                    pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                        return {
                            page: PaginationUtil.parsePage($stateParams.page),
                            sort: $stateParams.sort,
                            predicate: PaginationUtil.parsePredicate($stateParams.sort),
                            ascending: PaginationUtil.parseAscending($stateParams.sort),
                            search: $stateParams.search,
                            size: parseInt($stateParams.size)
                        };
                    }]
                }
            })
            .state('<%= inspeccionEntityHyphenCase %>.modal', {
                parent: '<%= inspeccionEntityHyphenCase %>',
                data: {
                    permissions: ['<%= inspeccionEntityManagementPermission %>'],
                    pageTitle: '<%= inspeccionEntityLabel %>'
                },
                abstract: true,
                onEnter: ['$stateParams', '$state', '$uibModal', 'EntityModalControllerConfig', function ($stateParams, $state, $uibModal, EntityModalControllerConfig) {
                    this.modalDialog = $uibModal.open({
                        templateUrl: 'app/entities/_components/entity-modal/templates/modal-base.html',
                        controller: 'EntityModalController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            modalControllerConfig: function () {
                                return new EntityModalControllerConfig('<%= inspeccionEntitySnakeCase %>');
                            }
                        }
                    });
                    this.modalDialog.result.then(function () {
                        $state.go('<%= inspeccionEntityHyphenCase %>', null, { reload: true });
                    }, function () {
                        $state.go('<%= inspeccionEntityHyphenCase %>', null, { reload: true });
                    });
                }],
                onExit: function () {
                    this.modalDialog.close();
                    this.modalDialog = null;
                }
            })
            .state('<%= inspeccionEntityHyphenCase %>.new', {
                parent: '<%= inspeccionEntityHyphenCase %>.modal',
                url: '/new',
                data: {
                    permissions: ['<%= inspeccionEntityManagementPermission %>']
                },
                views: {
                    'dialog-content@': {
                        templateUrl: 'app/entities/_components/entity-dialog/templates/dialog-base.html',
                        controller: '<%= inspeccionEntityClassCase %>DialogController',
                        controllerAs: 'vm'
                    }
                },
                ncyBreadcrumb: {
                    label: '<%= inspeccionEntityLabel %>'
                },
                resolve: {
                    entity: ['<%= inspeccionEntityClassCase %>', function (<%= inspeccionEntityClassCase %>) {
                        return new <%= inspeccionEntityClassCase %>({
                            fechaInspeccion: null,
                            fechaVerificacion: null,
                            vistoBuenoInterventor: null,
                            cumple: null,
                            estado: null,
                            id: null
                        });
                    }]
                }
            })
            .state('<%= inspeccionEntityHyphenCase %>.edit', {
                parent: '<%= inspeccionEntityHyphenCase %>.modal',
                url: '/{id}',
                data: {
                    permissions: ['<%= inspeccionEntityManagementPermission %>']
                },
                views: {
                    'dialog-content@': {
                        templateUrl: 'app/entities/_components/entity-dialog/templates/dialog-base.html',
                        controller: '<%= inspeccionEntityClassCase %>DialogController',
                        controllerAs: 'vm'
                    }
                },
                ncyBreadcrumb: {
                    label: '<%= inspeccionEntityLabel %>'
                },
                resolve: {
                    entity: ['$stateParams', '<%= inspeccionEntityClassCase %>', function ($stateParams, <%= inspeccionEntityClassCase %>) {
                        return <%= inspeccionEntityClassCase %>.get({ id: $stateParams.id }).$promise;
                    }]
                }
            })
            .state('<%= inspeccionEntityHyphenCase %>-confirmation', {
                parent: '<%= inspeccionEntityHyphenCase %>.edit',
                url: '/confirmacion',
                data: {
                    permissions: ['<%= inspeccionEntityManagementPermission %>'],
                    pageTitle: 'Confirmación'
                },
                views: {
                    'dialog-content@': {
                        templateUrl: 'app/entities/_components/entity-confirmation/templates/confirmation-base.html',
                        controller: 'EntityConfirmationController',
                        controllerAs: 'vm'
                    }
                },
                ncyBreadcrumb: {
                    label: 'Confirmación'
                },
                resolve: {
                    controllerConfig: ['EntityConfirmationControllerConfig', 'entity', '<%= inspeccionEntityClassCase %>', function (EntityConfirmationControllerConfig, entity, <%= inspeccionEntityClassCase %>) {
                        return new EntityConfirmationControllerConfig({
                            entity: entity,
                            entityService: <%= inspeccionEntityClassCase %>,
                            entityName: '<%= inspeccionEntityHyphenCase %>'
                        });
                    }]
                }
            });
    }

})();
