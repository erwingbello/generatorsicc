(function () {
    'use strict';

    angular
        .module('siccApp')
        .factory('<%= inspeccionEntityClassCase %>', <%= inspeccionEntityClassCase %>);

    <%= inspeccionEntityClassCase %>.$inject = ['$resource', 'DateUtils'];

    function <%= inspeccionEntityClassCase %>($resource, DateUtils) {
        var resourceUrl = 'api/<%= inspeccionEntityHyphenCase %>/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true },
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.fechaInspeccion = DateUtils.convertDateTimeFromServer(data.fechaInspeccion);
                    data.fechaVerificacion = DateUtils.convertDateTimeFromServer(data.fechaVerificacion);
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.fechaInspeccion = DateUtils.convertDateTimeFromServer(data.fechaInspeccion);
                    data.fechaVerificacion = DateUtils.convertDateTimeFromServer(data.fechaVerificacion);
                    return data;
                }
            },
            'finalize': {
                method: 'PUT',
                url: 'api/<%= inspeccionEntityHyphenCase %>/finalize'
            }
        });
    }
    
})();
