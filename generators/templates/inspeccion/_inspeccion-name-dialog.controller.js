(function () {
    'use strict';

    angular
        .module('siccApp')
        .controller('<%= inspeccionEntityClassCase %>DialogController', <%= inspeccionEntityClassCase %>DialogController);

    <%= inspeccionEntityClassCase %>DialogController.$inject = ['DialogInspeccionControllerFactory', '$scope', 'entity', '<%= inspeccionEntityClassCase %>'];

    function <%= inspeccionEntityClassCase %>DialogController(DialogInspeccionControllerFactory, $scope, entity, <%= inspeccionEntityClassCase %>) {
        var DialogInspeccionController = DialogInspeccionControllerFactory.create($scope, <%= inspeccionEntityClassCase %>, entity);
        var controller = new DialogInspeccionController({
            entityName: '<%= inspeccionEntityHyphenCase %>',
            withFinalizeState: true,
            withBackButton:false,
            // SICC:TODO Verificar en nombre del campo tramoDeInspeccionXXX en el controller de estado
            tramoFieldName: 'tramoDe<%= inspeccionEntityClassCase %>'
        });
        return controller;
    }

})();
