(function () {
    'use strict';

    angular
        .module('siccApp')
        .controller('<%= inspeccionEntityClassCase %>Controller', <%= inspeccionEntityClassCase %>Controller);

    <%= inspeccionEntityClassCase %>Controller.$inject = ['$filter', 'InspeccionesListControllerFactory', '$scope', 'pagingParams', '<%= inspeccionEntityClassCase %>'];

    function <%= inspeccionEntityClassCase %>Controller($filter, InspeccionesListControllerFactory, $scope, pagingParams, <%= inspeccionEntityClassCase %>) {
        var InspeccionesListController = InspeccionesListControllerFactory.create($scope, pagingParams, <%= inspeccionEntityClassCase %>);

        var controller = new InspeccionesListController({
            title: '<%= inspeccionEntityLabelPlural %>',
            entityName: '<%= inspeccionEntityHyphenCase %>',
            sortable: true,
            getColumnsConfig: function (controller) {
                return {
                    fields: [
                        {
                            sortBy: 'id',
                            label: "ID",
                            value: function (item) {
                                return item.id;
                            }
                        },
                        {
                            // SICC:TODO Verificar en nombre del campo tramoDeInspeccionXXX en el controller de inspeccion
                            sortBy: 'tramoDe<%= inspeccionEntityClassCase %>.nombreTramo',
                            label: "Tramo de inspección",
                            value: function (item) {
                                // SICC:TODO Verificar en nombre del campo tramoDeInspeccionXXX en el controller de inspeccion
                                return item.tramoDe<%= inspeccionEntityClassCase %> && item.tramoDe<%= inspeccionEntityClassCase %>.nombreTramo;
                            },
                            linkToState: function (item) {
                                return {
                                    state: 'tramo-detail',
                                    // SICC:TODO Verificar en nombre del campo tramoDeInspeccionXXX en el controller de inspeccion
                                    stateParams: { id: item.tramoDe<%= inspeccionEntityClassCase %> && item.tramoDe<%= inspeccionEntityClassCase %>.id }
                                };
                            }
                        },
                        {
                            sortBy: 'estado',
                            label: "Estado",
                            value: function (item) {
                                return item.estado;
                            }
                        },
                        {
                            sortBy: 'fechaInspeccion',
                            label: "Fecha de inspección",
                            value: function (item) {
                                return $filter('date')(item.fechaInspeccion, 'medium');
                            }
                        },
                        {
                            sortBy: 'fechaVerificacion',
                            label: "Fecha de verificación",
                            value: function (item) {
                                return $filter('date')(item.fechaVerificacion, 'medium');
                            }
                        }
                    ],
                    editLink: function (item) {
                        return {
                            state: controller.getOptions().entityName + '.edit',
                            stateParams: { id: item.id }
                        };
                    }
                };
            }
        });

        return controller;
    }

})();
