(function() {
    'use strict';

    angular
        .module('siccApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('reporte-<%= inspeccionSlug %>', {
            parent: 'reportes',
            url: '/<%= inspeccionSlug %>',
            data: {
                permissions: ['<%= reporteEstadoEntityManagementPermission %>'],
                
                pageTitle: 'Reporte - <%= inspeccionEntityLabel %>'
            },
            views: {
                'content@': {
                    templateUrl: 'app/reportes/reporte-<%= inspeccionSlug %>/reporte-<%= inspeccionSlug %>.html',
                    controller: 'ReportesMantenimientoController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                service: '<%= inspeccionEntityClassCase %>ReporteService',
                csvNamePrefix: function() {
                    return '<%= reporteCSVPrefix %>';
                }
            }
        });
    }
})();
