(function() {
    'use strict';
    angular
        .module('siccApp')
        .factory('Archivo<%= inspeccionEntityClassCase %>', Archivo<%= inspeccionEntityClassCase %>);

    Archivo<%= inspeccionEntityClassCase %>.$inject = ['$resource', 'DateUtils'];

    function Archivo<%= inspeccionEntityClassCase %> ($resource, DateUtils) {
        var resourceUrl =  'api/archivo-<%= inspeccionSlug %>/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.fechaDeRegistro = DateUtils.convertDateTimeFromServer(data.fechaDeRegistro);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
