(function() {
    'use strict';

    angular
        .module('siccApp')
        .controller('Archivo<%= inspeccionEntityClassCase %>DeleteController',Archivo<%= inspeccionEntityClassCase %>DeleteController);

    Archivo<%= inspeccionEntityClassCase %>DeleteController.$inject = ['$uibModalInstance', 'entity', 'Archivo<%= inspeccionEntityClassCase %>'];

    function Archivo<%= inspeccionEntityClassCase %>DeleteController($uibModalInstance, entity, Archivo<%= inspeccionEntityClassCase %>) {
        var vm = this;
        vm.archivo<%= inspeccionEntityClassCase %> = entity;
        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        vm.confirmDelete = function (id) {
            Archivo<%= inspeccionEntityClassCase %>.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };
    }
})();
