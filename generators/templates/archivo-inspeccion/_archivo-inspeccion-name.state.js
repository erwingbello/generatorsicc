(function() {
    'use strict';

    angular
        .module('siccApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider', 'paginationConstants'];

    function stateConfig($stateProvider, paginationConstants) {
        $stateProvider
        .state('archivo-<%= inspeccionSlug %>', {
            parent: '<%= estadoSlug %>.edit',
            url: '/archivo?filesPage&filesSort&filesSearch&filesCount',
            data: {
                permissions: ['<%= inspeccionEntityManagementPermission %>'],
                
                pageTitle: 'Archivos del estado de inspección'
            },
            ncyBreadcrumb: {
                label: 'Archivos adjuntos'
            },
            views: {
                'dialog-content@': {
                    templateUrl: 'app/entities/_components/entity-list/templates/list-base.html',
                    controller: 'Archivo<%= inspeccionEntityClassCase %>Controller',
                    controllerAs: 'vm'
                }
            },
            params: {
                filesPage: {
                    value: '1',
                    squash: true
                },
                filesSort: {
                    value: 'id,asc',
                    squash: true
                },
                filesSearch: null,
                filesCount: {
                    value: paginationConstants.itemsPerPage.toString(),
                    squash: true
                }
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.filesPage),
                        sort: $stateParams.filesSort,
                        predicate: PaginationUtil.parsePredicate($stateParams.filesSort),
                        ascending: PaginationUtil.parseAscending($stateParams.filesSort),
                        search: $stateParams.filesSearch,
                        size: parseInt($stateParams.filesCount)
                    };
                }],
                <%= estadoEntityCammelCase %>: ['entity', function(entity){
                    return entity;
                }],
                inspeccionEntity: ['inspeccionEntity', function(inspeccionEntity){
                    return inspeccionEntity;
                }],
                archivosListListener:['EntityListener', function(EntityListener){
                    return EntityListener.make();
                }]
            }
        })
        .state('archivo-<%= inspeccionSlug %>.delete', {
            parent: 'archivo-<%= inspeccionSlug %>',
            url: '/{id_archivo}/delete',
            data: {
                permissions: ['<%= inspeccionEntityManagementPermission %>']
                
            },
            onEnter: ['$stateParams', '$state', '$uibModal', 'archivosListListener', function($stateParams, $state, $uibModal, archivosListListener) {
                $uibModal.open({
                    templateUrl: 'app/entities/archivo-<%= inspeccionSlug %>/archivo-<%= inspeccionSlug %>-delete-dialog.html',
                    controller: 'Archivo<%= inspeccionEntityClassCase %>DeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Archivo<%= inspeccionEntityClassCase %>', function(Archivo<%= inspeccionEntityClassCase %>) {
                            return Archivo<%= inspeccionEntityClassCase %>.get({id : $stateParams.id_archivo});
                        }]
                    }
                }).result.then(function() {
                    $state.go('^');
                    archivosListListener.dispatch();
                }, function() {
                    $state.go('^');
                    archivosListListener.dispatch();
                });
            }]
        })
        .state('archivo-<%= inspeccionSlug %>.upload-file', {
            parent: 'archivo-<%= inspeccionSlug %>',
            url: '/upload-file',
            data: {
                permissions: ['<%= inspeccionEntityManagementPermission %>']
                
            },
            ncyBreadcrumb: {
                skip: true // Never display this state in breadcrumb.
            },
            onEnter: ['$stateParams', '$state', '$uibModal', 'archivosListListener', 'EntityFileUploadControllerConfig', function ($stateParams, $state, $uibModal, archivosListListener, EntityFileUploadControllerConfig) {
                this.modal=$uibModal.open({
                    templateUrl: 'app/entities/_components/entity-file-uploader/templates/modal-file-upload.html',
                    controller: 'EntityFileUploadController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'md',
                    resolve: {
                        controllerConfig:function(){
                            return new EntityFileUploadControllerConfig({
                                resourceURL:'api/archivo-<%= inspeccionSlug %>',
                                uploadSuccessEventName:'siccApp:archivo<%= inspeccionEntityClassCase %>Update',
                                fileParamName:'archivo',
                                requestParams:{
                                    idArchivo:"",
                                    idEstado:$stateParams.id_<%= estadoEntitySnakeCase %>
                                }
                            });
                        }
                    }
                });
                this.modal.result.then(function(){
                    archivosListListener.dispatch();
                    $state.go('^');
                }, function(reason){
                    archivosListListener.dispatch();
                    if(reason==='force-close') return;
                    $state.go('^');
                });
            }],
            onExit:function(){
                var that=this;
                //forzar cierre por cambio de url, y no dejar el dialogo abierto
                that.modal.opened.then(function(){
                    //forzar cierre por si se instancia modal porque la url 
                    //y simultaneamente se hace redirect a otro state (bloqueo de acceso) ya que no se alcanza a cerrar
                    that.modal.dismiss('force-close');
                });
                
            }
        });
    }

})();
