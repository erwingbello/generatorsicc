(function() {
    'use strict';

    angular
        .module('siccApp')
        .controller('Archivo<%= inspeccionEntityClassCase %>Controller', Archivo<%= inspeccionEntityClassCase %>Controller);

    Archivo<%= inspeccionEntityClassCase %>Controller.$inject = ['ArchivosListControllerFactory', '$scope', 'pagingParams', 'Archivo<%= inspeccionEntityClassCase %>', '<%= estadoEntityCammelCase %>', 'archivosListListener', 'inspeccionEntity'];

    function Archivo<%= inspeccionEntityClassCase %>Controller (ArchivosListControllerFactory, $scope, pagingParams, Archivo<%= inspeccionEntityClassCase %>, <%= estadoEntityCammelCase %>, archivosListListener, inspeccionEntity) {

        var ArchivosListController=ArchivosListControllerFactory.create($scope, pagingParams, Archivo<%= inspeccionEntityClassCase %>, <%= estadoEntityCammelCase %>, archivosListListener, inspeccionEntity);
        var controller=new ArchivosListController({
            entityName:"archivo-<%= inspeccionSlug %>",
            parentEntityType:"<%= estadoSlug %>",
            parentFilterParamName:"idEstado"
            
        });
        return controller;
    }
})();
