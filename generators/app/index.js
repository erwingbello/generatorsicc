//Extending generator

var generators = require('yeoman-generator');
var Promise = require('promise');

module.exports = generators.Base.extend({
  // The name `constructor` is important here
  constructor: function () {
    // Calling the super constructor is important so our generator is correctly set up
    generators.Base.apply(this, arguments);


    
  },

  myMethod:function(){
    this.log("It's an empty generator, please use subgenerators");
  }
});