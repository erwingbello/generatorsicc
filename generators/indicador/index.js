var generators = require('yeoman-generator');
var _ = require('lodash');
var Promise = require('promise');
var utils = require('../utils');
var chalk = require('chalk');

module.exports = generators.Base.extend({
    // The name `constructor` is important here
    constructor: function() {
        // Calling the super constructor is important so our generator is correctly set up
        generators.Base.apply(this, arguments);


        this.argument('indicatorLabel', { type: String, required: true, desc: 'Nombre legible del indicador' });
        this.argument('indicatorSlug', { type: String, required: true, desc: 'slug del indicador' });

        this.option('bulk', {type:Boolean, desc:"Indique si el indicador tiene carga masiva"});


        this.entityWithBulk=this.options.bulk;
        this.appFolder="src/main/webapp/app";
        this.entitiesFolder=this.appFolder+"/entities";
        this.reportesFolder=this.appFolder+"/reportes";

    },

    

    configuring: {
        initInspeccionValues: function() {


            this.sourceRoot(this.sourceRoot()+'/../../templates');


            // this.log('indicatorLabel \t'+ this.indicatorLabel);
            // this.log('indicatorSlug \t'+ this.indicatorSlug);
            // this.log('entityName \t'+ _.camelCase(this.entityName));
            // this.log('entityName \t'+ _.kebabCase(this.entityName));
            // this.log('entityName \t'+ _.snakeCase(this.entityName));
            // this.log('entityName \t'+ _.upperFirst(_.camelCase(this.entityName)));
            var inspeccionSlug='inspeccion-'+this.indicatorSlug;
            this.inspeccionSlug=inspeccionSlug;
            this.inspeccionEntityLabel=_.capitalize('Inspección de '+this.indicatorLabel);
            this.inspeccionEntityLabelPlural=_.capitalize('Inspecciones de '+this.indicatorLabel);
            this.inspeccionEntityClassCase=_.upperFirst(_.camelCase(this.inspeccionSlug));
            this.inspeccionEntityCammelCase=_.camelCase(this.inspeccionSlug);
            this.inspeccionEntityHyphenCase=_.kebabCase(this.inspeccionSlug);
            this.inspeccionEntitySnakeCase=_.snakeCase(this.inspeccionSlug);
            this.inspeccionEntityManagementPermission='gestion_'+this.entityCode;


            // this.log('inspeccionEntityLabel \t'+this.inspeccionEntityLabel);
            // this.log('inspeccionEntityClassCase \t'+this.inspeccionEntityClassCase);
            // this.log('inspeccionEntityCammelCase \t'+this.inspeccionEntityCammelCase);
            // this.log('inspeccionEntityHyphenCase \t'+this.inspeccionEntityHyphenCase);
            // this.log('inspeccionEntitySnakeCase \t'+this.inspeccionEntitySnakeCase);
            // this.log('inspeccionEntityManagementPermission \t'+this.inspeccionEntityManagementPermission);
        },
        initEstadosValues: function() {
            // this.log('indicatorLabel \t'+ this.indicatorLabel);
            // this.log('indicatorSlug \t'+ this.indicatorSlug);
            // this.log('entityName \t'+ _.camelCase(this.entityName));
            // this.log('entityName \t'+ _.kebabCase(this.entityName));
            // this.log('entityName \t'+ _.snakeCase(this.entityName));
            // this.log('entityName \t'+ _.upperFirst(_.camelCase(this.entityName)));
            var estadoSlug='estado-'+this.indicatorSlug;
            this.estadoSlug=estadoSlug;
            this.estadoEntityClassCase=_.upperFirst(_.camelCase(this.estadoSlug));
            this.estadoEntityLabel=_.capitalize('Estado de '+this.indicatorLabel);
            this.estadoEntityLabelPlural=_.capitalize('Estados de '+this.indicatorLabel);
            this.estadoEntitySnakeCase=_.snakeCase(this.estadoSlug);
            this.estadoEntityCammelCase=_.camelCase(this.estadoSlug);
            // this.inspeccionEntityLabel=_.capitalize('Inspección de '+this.indicatorLabel);
            // this.inspeccionEntityLabelPlural=_.capitalize('Inspecciones de '+this.indicatorLabel);
            // this.inspeccionEntityClassCase=_.upperFirst(_.camelCase(this.inspeccionSlug));
            // this.inspeccionEntityCammelCase=_.camelCase(this.inspeccionSlug);
            // this.inspeccionEntityHyphenCase=_.kebabCase(this.inspeccionSlug);
            // this.inspeccionEntitySnakeCase=_.snakeCase(this.inspeccionSlug);
            // this.inspeccionEntityManagementPermission='gestion_'+this.entityCode;


            // this.log('estadoSlug \t'+this.estadoSlug);
            // this.log('inspeccionEntityLabel \t'+this.inspeccionEntityLabel);
            // this.log('inspeccionEntityClassCase \t'+this.inspeccionEntityClassCase);
            // this.log('inspeccionEntityCammelCase \t'+this.inspeccionEntityCammelCase);
            // this.log('inspeccionEntityHyphenCase \t'+this.inspeccionEntityHyphenCase);
            // this.log('inspeccionEntitySnakeCase \t'+this.inspeccionEntitySnakeCase);
            // this.log('inspeccionEntityManagementPermission \t'+this.inspeccionEntityManagementPermission);
        },
        initArchivosValues: function() {
            
        },
        initReporteValues: function() {
            this.reporteEstadoServicePrefix=_.replace(_.kebabCase(this.estadoSlug),'-','');
            this.reporteEstadoEntityManagementPermission='reporte_'+this.entityCode;
            this.reporteCSVPrefix=('reporte_'+this.entityCode+'_').toLowerCase();
        }
    },

    //prompting:{},// - Where you prompt users for options (where you'd call this.prompt())
    //configuring:{},// - Saving configurations and configure the project (creating .editorconfig files and other metadata files)
    // default: {
    //     methodA: function() { console.log('method A run') }
    // }, // - If the method name doesn't match a priority, it will be pushed to this group.
    //writing:{},// - Where you write the generator specific files (routes, controllers, etc)
    //conflicts:{},// - Where conflicts are handled (used internally)
    //install:{},// - Where installation are run (npm, bower)
    writing:{
        addInspeccionToMenu: function(){
            try {
                var fullPath = this.appFolder + '/layouts/navbar/navbar.html';
                utils.rewriteFile({
                    file: fullPath,
                    needle: 'generador-sicc-add-menu-inspeccion-'+this.inspeccionType,
                    splicable: [
                        '\n'+
                        '                            <!-- SICC:TODO Verificar que el menu item quede bien insertado en el markup, no olvidar complementar el navbar controller-->\n' +
                        '                            <li has-any-authority=" {{vm.roles.'+this.inspeccionEntityManagementPermission+'}}" ui-sref-active="active">\n' +
                        '                                <a ui-sref="' + this.inspeccionSlug + '" ng-click="vm.collapseNavbar()" id="ingreso'+_.upperFirst(_.camelCase(this.indicatorLabel))+'-menu">\n' +
                        '                                    <span class="fa fa-pencil-square-o"></span>&nbsp;\n' +
                        '                                    <span>('+this.entityCode+') '+this.indicatorLabel+'</span>\n' +
                        '                                </a>\n' +
                        '                            </li>'
                    ]
                }, this);
            } catch (e) {
                this.log(chalk.yellow('\nNo se encontro el menunavbar ') + fullPath);
                this.log(e);
            }
        },
        addReporteInspeccionToMenu: function(){
            try {
                var fullPath = this.appFolder + '/layouts/navbar/navbar.html';
                utils.rewriteFile({
                    file: fullPath,
                    needle: 'generador-sicc-add-menu-reporte-inspeccion-'+this.inspeccionType,
                    splicable: [
                        '\n'+
                        '                            <!-- SICC:TODO Verificar que el menu item quede bien insertado en el markup, no olvidar complementar el navbar controller-->\n' +
                        '                            <li has-any-authority=" {{vm.roles.'+this.reporteEstadoEntityManagementPermission+'}}" ui-sref-active="active">\n' +
                        '                                <a ui-sref="reporte-' + this.inspeccionSlug + '" ng-click="vm.collapseNavbar()" id="reporte'+_.upperFirst(_.camelCase(this.indicatorLabel))+'-menu">\n' +
                        '                                    <span class="fa fa-pie-chart"></span>&nbsp;\n' +
                        '                                    <span>('+this.entityCode+') '+this.indicatorLabel+'</span>\n' +
                        '                                </a>\n' +
                        '                            </li>'
                    ]
                }, this);
            } catch (e) {
                this.log(chalk.yellow('\nNo se encontro el menunavbar ') + fullPath);
                this.log(e);
            }
        },
        generateInspeccionFiles: function(){
            // this.log("sourceRoot: "+this.sourceRoot())


            var nameFolder=this.entitiesFolder+'/'+this.inspeccionSlug;
            this.template('inspeccion/_inspeccion-name.state.js', nameFolder+'/'+this.inspeccionSlug+'.state.js');
            this.template('inspeccion/_inspeccion-name.service.js', nameFolder+'/'+this.inspeccionSlug+'.service.js');
            this.template('inspeccion/_inspeccion-name.controller.js', nameFolder+'/'+this.inspeccionSlug+'.controller.js');
            this.template('inspeccion/_inspeccion-name-dialog.controller.js', nameFolder+'/'+this.inspeccionSlug+'-dialog.controller.js');
            this.template('inspeccion/_inspeccion-name-dialog-fields.html', nameFolder+'/'+this.inspeccionSlug+'-dialog-fields.html');
            this.template('inspeccion/_inspeccion-name-confirmation.html', nameFolder+'/'+this.inspeccionSlug+'-confirmation.html');
        },
        generateEstadosFiles: function(){


            var nameFolder=this.entitiesFolder+'/'+this.estadoSlug;
            this.template('estado/_estado-name.state.js', nameFolder+'/'+this.estadoSlug+'.state.js');
            this.template('estado/_estado-name.service.js', nameFolder+'/'+this.estadoSlug+'.service.js');
            this.template('estado/_estado-name.controller.js', nameFolder+'/'+this.estadoSlug+'.controller.js');
            if(this.entityWithMedidas)
                this.template('estado/_estado-name-medidas.controller.js', nameFolder+'/'+this.estadoSlug+'-medidas.controller.js');
            this.template('estado/_estado-name-dialog.controller.js', nameFolder+'/'+this.estadoSlug+'-dialog.controller.js');
            this.template('estado/_estado-name-dialog-fields.html', nameFolder+'/'+this.estadoSlug+'-dialog-fields.html');
            if(!this.entityWithMedidas)
                this.template('estado/_estado-name-confirmation.html', nameFolder+'/'+this.estadoSlug+'-confirmation.html');
        },
        generateArchivosInspeccionFiles: function(){


            var nameFolder=this.entitiesFolder+'/archivo-'+this.inspeccionSlug;
            this.template('archivo-inspeccion/_archivo-inspeccion-name.state.js', nameFolder+'/archivo-'+this.inspeccionSlug+'.state.js');
            this.template('archivo-inspeccion/_archivo-inspeccion-name.service.js', nameFolder+'/archivo-'+this.inspeccionSlug+'.service.js');
            this.template('archivo-inspeccion/_archivo-inspeccion-name.controller.js', nameFolder+'/archivo-'+this.inspeccionSlug+'.controller.js');
            this.template('archivo-inspeccion/_archivo-inspeccion-name-delete-dialog.html', nameFolder+'/archivo-'+this.inspeccionSlug+'-delete-dialog.html');
            this.template('archivo-inspeccion/_archivo-inspeccion-name-delete-dialog.controller.js', nameFolder+'/archivo-'+this.inspeccionSlug+'-delete-dialog.controller.js');
            
        },
        generateReporteInspeccionFiles: function(){

            var nameFolder=this.reportesFolder+'/'+(this.entityCode).toLowerCase()+'-reporte-'+this.inspeccionSlug;
            this.template('reporte/_reporte-inspeccion-name.html', nameFolder+'/reporte-'+this.inspeccionSlug+'.html');
            this.template('reporte/_reporte-inspeccion-name.service.js', nameFolder+'/reporte-'+this.inspeccionSlug+'.service.js');
            this.template('reporte/_reporte-inspeccion-name.state.js', nameFolder+'/reporte-'+this.inspeccionSlug+'.state.js');
            
        }
    },
    end: function() {
        //tambien pueden quedar no en forma de objeto sino como funcion
        this.spawnCommand('gulp', ['inject']);
        console.log('ending generator')
    }, // - Called last, cleanup, say good bye, etc
    // prompting: function() {
    //     return this.prompt([{
    //         type: 'input',
    //         name: 'name',
    //         message: 'Your project name',
    //         default: this.appname // Default to current folder name
    //     }, {
    //         type: 'confirm',
    //         name: 'cool',
    //         message: 'Would you like to enable the Cool feature?'
    //     }, {
    //         type: 'input',
    //         name: 'username',
    //         message: 'What\'s your Github username',
    //         store: true //Para recordar o establecer por defecto en una proxima ejecucion
    //     }]).then(function(answers) {
    //         this.log('app name', answers.name);
    //         this.log('cool feature', answers.cool);
    //     }.bind(this));
    // }
    prompting: function() {
        var indicatorSlug=this.indicatorSlug;
        return this.prompt([{
            type: 'list',
            name: 'inspeccionType',
            message: 'Que tipo de inspeccion?',
            choices:[
                {name:'Pavimentos rigidos', value:'pavimentos-rigidos'},
                {name:'Pavimentos flexibles', value:'pavimentos-flexibles'},
                {name:'Estructuras', value:'estructuras'}
            ],
            store   : true
        },{
            type: 'input',
            name: 'entityCode',
            message: 'El código del indicador'
        },{
            type: 'confirm',
            name: 'entitySlug',
            message: 'El slug del indicador es: "'+indicatorSlug+'"?'
        },{
            type: 'confirm',
            name: 'entityWithMedidas',
            message: 'El indicador usa medidas?'
        }]).then(function(answers) {

            this.entityCode=answers.entityCode;
            this.inspeccionType=answers.inspeccionType;
            this.entityWithMedidas=answers.entityWithMedidas;
            if(!answers.entitySlug)
                process.exit(1);
            if(!answers.entityCode){
                this.log('El código del indicador es vacio');
                process.exit(1);
            }
            if(!answers.inspeccionType){
                this.log('El tipo de inspección del indicador es vacio');
                process.exit(1);
            }
        }.bind(this));
    }
});
