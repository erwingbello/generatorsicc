#**generator-sicc**#

> **Descripción:**

> - Este es un plugin para yeoman, para generar archivos de front para indicadores de la aplicación SICC.
> - Se requiere tener instalado **npm**, **git** y **yeoman**

### **npm** ###
tener npm instalado

para actualizar
> npm install --global npm@latest 

### **git** ###
Git instalado
> git --version


### **yeoman** ###
Instala globalmente a yeoman

> npm install --global yo

Comando yo, sirve para (> yo y seguir preguntas):
- buscar generadores  ``` yo --generators ```
- instalar generadores
- actualizar generadores

El siguiente comando sirve para revisar y arreglar problemas del generador    
> yo doctor


## **Setup** ##
> npm install --global "https://erwingbello@bitbucket.org/erwingbello/generatorsicc.git"


## **Run** ##
Para generar un indicador correr el commando, en la raiz del proyecto **src/main/webapp/app**

> yo sicc:indicador "Juntas" juntas --bulk

Donde la opción --bulk indica que el indicador tiene carga masiva

Seguido, responder las preguntas:

```
? Que tipo de inspeccion? (Use arrow keys)
> Pavimentos rigidos
  Pavimentos flexibles
  Estructuras
```

```
>? El código del indicador
```
 
```
>? El slug del indicador es: "juntas"? (Y/n)
```

```
>? El indicador usa medidas? (Y/n) Y
```
 
